# Dotnet 7 playwright container

The existing Docker Playwright container currently only provides the dotnet 6.0 SDK, I need the dotnet 7.0 SDK. This is a modification of that existing container

See the original at <https://github.com/microsoft/playwright-dotnet/>

This repository is entirely at your own risk. It exists to fulfill my own needs, and I may stop maintaining it at any point.
