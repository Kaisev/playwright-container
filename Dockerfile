# Custom .net 7 image as Playwright team only use .net LTS for docker images: https://github.com/microsoft/playwright-dotnet/issues/2466
# Official build steps for LTS Docker image: https://github.com/microsoft/playwright-dotnet/blob/main/utils/docker/Dockerfile.focal

FROM mcr.microsoft.com/dotnet/sdk:7.0-jammy as base

RUN apt-get update && \
    # Feature-parity with node.js base images.
    apt-get install -y --no-install-recommends git openssh-client curl gpg node.js && \
    # clean apt cache
    rm -rf /var/lib/apt/lists/*

ENV PLAYWRIGHT_BROWSERS_PATH=/ms-playwright
RUN mkdir /ms-playwright && \
    mkdir /ms-playwright-agent && \
    cd /ms-playwright-agent && \
    dotnet new console && \
    echo '<?xml version="1.0" encoding="utf-8"?><configuration><packageSources><add key="local" value="/tmp/"/></packageSources></configuration>' > nuget.config && \
    dotnet add package Microsoft.Playwright && \
    dotnet build && \
    ./bin/Debug/net7.0/playwright.ps1 install --with-deps && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/* && \
    rm -rf /ms-playwright-agent && \
    chmod -R 777 /ms-playwright

